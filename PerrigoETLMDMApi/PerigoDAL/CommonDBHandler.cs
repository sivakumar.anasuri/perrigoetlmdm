﻿using Npgsql;
using System;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;

namespace PerigoDAL
{

    public abstract class CommonDBHandler
    {
        private string _connectionstring;
        private string _commandtext;
        private CommandType _commandtype;
        public CommonDBHandler(string ConnectionString, string CommandText, CommandType CommandType, params DbParameter[] Parameters)
        {
            _connectionstring = ConnectionString;
            _commandtext = CommandText;
            _commandtype = CommandType;

            connectionHandle = GetConnection();
            connectionHandle.ConnectionString = _connectionstring;
            dataadapterHandle = GetDataAdapter();
            dataadapterHandle.SelectCommand.CommandText = _commandtext;
            dataadapterHandle.SelectCommand.CommandType = _commandtype;

           
            commandHandle = GetCommand();
            commandHandle.CommandText = _commandtext;
            commandHandle.CommandType = _commandtype;

            foreach (DbParameter p in Parameters)
            {
                dataadapterHandle.SelectCommand.Parameters.Add(p);
               // commandHandle.Parameters.Add(p);
            }
        }
        public virtual IDbConnection GetConnection()
        { throw new Exception("Please define specific GetConnection method"); }

        public virtual IDbCommand GetCommand()
        { throw new Exception("Please define specific GetCommand method"); }

        public virtual IDbDataAdapter GetDataAdapter()
        { throw new Exception("Please define specific DataAdapter method"); }


        protected IDbConnection connectionHandle;
        protected IDbDataAdapter dataadapterHandle;
        protected IDbCommand commandHandle;
         

        public DataSet GetDataset()
        {
            connectionHandle.Open();
            DataSet ds = new DataSet();
            dataadapterHandle.Fill(ds);
            //commandHandle.ExecuteNonQuery();
            connectionHandle.Close();
            return ds;
        }
        public int ExecuteCommand()
        {
            connectionHandle.Open();
            int ret= commandHandle.ExecuteNonQuery();
            connectionHandle.Close();
            return ret;
        }


    }

    public class CommonDbParameter : DbParameter,IDbDataParameter
    {
        public CommonDbParameter(string ParameterName, object Value)
        {
            this.ParameterName = ParameterName;
            this.Value = Value;
        }

        public override string ParameterName
        {
            get;
            set;
        }

        public override object Value
        {
            get;
            set;
        }

        public override DbType DbType
        {
            get;
            set;
        }

        public override ParameterDirection Direction
        {
            get;
            set;
        }

        public override bool IsNullable
        {
            get;
            set;
        }


        public override int Size
        {
            get;
            set;
        }

        public override string SourceColumn
        {
            get;
            set;
        }

        public override bool SourceColumnNullMapping
        {
            get;
            set;
        }

        public override DataRowVersion SourceVersion
        {
            get;
            set;
        }

        public override void ResetDbType()
        {
           // base.ResetDbType();
        }

        public byte Precision
        {
            get;
            set;
        }

        public byte Scale
        {
            get;
            set;
        }
    }

    public class SQLHandler : CommonDBHandler
    {
        public SQLHandler(string ConnectionString, string CommandText, CommandType CommandType, params SqlParameter[] SqlParameters)
            : base(ConnectionString, CommandText, CommandType, SqlParameters)
        {
            
        }
        public override IDbConnection GetConnection()
        { return (IDbConnection)new SqlConnection(); }

        public override IDbCommand GetCommand()
        { return (IDbCommand)new SqlCommand("", (SqlConnection)connectionHandle); }

        public override IDbDataAdapter GetDataAdapter()
        { return (IDbDataAdapter)new SqlDataAdapter("", (SqlConnection)connectionHandle); }

        

    }

    public class OracleDBHandler : CommonDBHandler
    {
        public OracleDBHandler(string ConnectionString, string CommandText, CommandType CommandType, params OracleParameter[] SqlParameters)
            : base(ConnectionString, CommandText, CommandType, SqlParameters)
        {

        }
        public override IDbConnection GetConnection()
        { return (IDbConnection)new OracleConnection(); }

        public override IDbCommand GetCommand()
        { return (IDbCommand)new OracleCommand("", (OracleConnection)connectionHandle); }

        public override IDbDataAdapter GetDataAdapter()
        { return (IDbDataAdapter)new OracleDataAdapter("", (OracleConnection)connectionHandle); }



    }


    public class PostgreSQLHandler : CommonDBHandler
    {
        public PostgreSQLHandler(string ConnectionString, string CommandText, CommandType CommandType, params DbParameter[] SqlParameters)
            : base(ConnectionString, CommandText, CommandType, SqlParameters)
        {

        }
        public override IDbConnection GetConnection()
        { return (IDbConnection)new NpgsqlConnection(); }

        public override IDbCommand GetCommand()
        { return (IDbCommand)new NpgsqlCommand("", (NpgsqlConnection)connectionHandle); }

        public override IDbDataAdapter GetDataAdapter()
        { return (IDbDataAdapter)new NpgsqlDataAdapter("", (NpgsqlConnection)connectionHandle); }



    }

    public class OdbcHandler : CommonDBHandler
    {
        public OdbcHandler(string ConnectionString, string CommandText, CommandType CommandType, params OdbcParameter[] SqlParameters)
            : base(ConnectionString, CommandText, CommandType, SqlParameters)
        {

        }
        public override IDbConnection GetConnection()
        { return (IDbConnection)new OdbcConnection(); }

        public override IDbCommand GetCommand()
        { return (IDbCommand)new OdbcCommand("", (OdbcConnection)connectionHandle); }

        public override IDbDataAdapter GetDataAdapter()
        { return (IDbDataAdapter)new OdbcDataAdapter("", (OdbcConnection)connectionHandle); }



    }

}
