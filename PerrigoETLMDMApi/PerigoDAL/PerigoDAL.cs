﻿using System;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Npgsql;
using System.Data.Odbc;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;

namespace PerigoDAL
{
    public enum DBType
    {
        Sql,
        PostgreSql,
        RedShift,
        Oracle
    }
    public class DAL
    {
        private static DBType DbType = (DBType)Enum.Parse(typeof(DBType), ConfigurationManager.AppSettings["DbType"], true);
        public static DataSet ExecuteSQL(string sql, string ConnectionString, CommandType CommandType, params CommonDbParameter[] Sqlparms)
        {
            CommonDBHandler Handler;

            if (DbType == DBType.Sql)
            {
                Handler = new SQLHandler(ConnectionString, sql, CommandType, Sqlparms.Select(i => new SqlParameter(i.ParameterName, i.Value)).ToArray());
            }
            else if (DbType == DBType.PostgreSql)
            {
                Handler = new PostgreSQLHandler(ConnectionString, sql, CommandType, Sqlparms.Select(i => new NpgsqlParameter(i.ParameterName, i.Value)).ToArray());
            }
            else if (DbType == DBType.RedShift)
            {
                Handler = new OdbcHandler(ConnectionString, sql, CommandType, Sqlparms.Select(i => new OdbcParameter(i.ParameterName, i.Value)).ToArray());
            }
            else if (DbType == DBType.Oracle)
            {
                Handler = new OracleDBHandler(ConnectionString, sql, CommandType, Sqlparms.Select(i => new OracleParameter(i.ParameterName, i.Value)).ToArray());
            }
            else
            {
                Handler = new SQLHandler("", "", CommandType, Sqlparms.Cast<SqlParameter>().ToArray());
            }
            return Handler.GetDataset();
        }
        public static int ExecuteCommand(string sql, string ConnectionString, CommandType CommandType, params CommonDbParameter[] Sqlparms)
        {
            CommonDBHandler Handler;

            if (DbType == DBType.Sql)
            {
                Handler = new SQLHandler(ConnectionString, sql, CommandType, Sqlparms.Select(i => new SqlParameter(i.ParameterName, i.Value)).ToArray());
            }
            else if (DbType == DBType.PostgreSql)
            {
                Handler = new PostgreSQLHandler(ConnectionString, sql, CommandType, Sqlparms.Select(i => new NpgsqlParameter(i.ParameterName, i.Value)).ToArray());
            }
            else if (DbType == DBType.RedShift)
            {
                Handler = new OdbcHandler(ConnectionString, sql, CommandType, Sqlparms.Select(i => new OdbcParameter(i.ParameterName, i.Value)).ToArray());
            }
            else if (DbType == DBType.Oracle)
            {
                Handler = new OracleDBHandler(ConnectionString, sql, CommandType, Sqlparms.Select(i => new OracleParameter(i.ParameterName, i.Value)).ToArray());
            }
            else
            {
                Handler = new SQLHandler("", "", CommandType, Sqlparms.Cast<SqlParameter>().ToArray());
            }
            return Handler.ExecuteCommand();
        }
        

    }
}
