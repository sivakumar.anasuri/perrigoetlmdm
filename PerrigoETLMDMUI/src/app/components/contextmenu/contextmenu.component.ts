import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-contextmenu',
  templateUrl: './contextmenu.component.html',
  styleUrls: ['./contextmenu.component.css']
})
export class ContextmenuComponent implements OnInit {

  constructor() { }
  @Input() top: any;
  @Input() left: any;
  @Input() Items: any[];
  @Input() show:boolean;
  ngOnInit() {
   
  }

}
