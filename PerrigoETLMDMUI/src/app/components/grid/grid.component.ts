import {DecimalPipe} from '@angular/common';
import {Component, QueryList, ViewChildren} from '@angular/core';
import {Observable} from 'rxjs';

import {NgbdSortableHeader, SortEvent} from './grid.directive';
import { GridService } from './grid.service';
import { ContextmenuComponent } from '../contextmenu/contextmenu.component';
import { MenuItem } from 'primeng/components/common/menuitem';




@Component(
    {
      selector: 'app-grid',
      templateUrl: './grid.component.html',
      styleUrls: ['./grid.component.css'],
      providers: [GridService, DecimalPipe]
    })
export class GridComponent {
  data$: Observable<any[]>;
  total$: Observable<number>;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;
  public ContextMenuItems:any[];
  public ShowContextmenu :boolean;
  public left:any;
  public top:any;
  items: MenuItem[];
  selectedid:any;
  
  //e:\Perigo\ETLMDMApi\PerrigoETLMDM\PerrigoETLMDMUI\node_modules\primeng\components\dom\domhandler.js

  constructor(public service: GridService) {
    this.total$ = service.total$;
    this.data$ = service.data$;
  }

  ngOnInit() {
    this.ContextMenuItems=[{Name:'Edit',Action:'Edit'},{Name:'Delete',Action:'Delete'}];
   
  }

 

  onContext(event,item)
  {
    this.selectedid=item.id;
    this.ShowContextmenu=true;
    this.left=event.clientX;
    this.top=event.clientY;
    return false;

  }
  
  
  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
}
