import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridComponent } from './grid/grid.component';
import { NgbdSortableHeader } from './grid/grid.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ContextmenuComponent } from './contextmenu/contextmenu.component';
import {ContextMenuModule} from 'primeng/contextmenu';
import {MenuItem} from 'primeng/api';


@NgModule({ 
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ContextMenuModule
    
    
  ],
  declarations: [GridComponent, NgbdSortableHeader, ContextmenuComponent],
  exports: [GridComponent,ContextmenuComponent],
  bootstrap: [GridComponent]
})
export class ComponentsModule { }
