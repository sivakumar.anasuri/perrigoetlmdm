import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobsComponent } from './jobs/jobs.component';
import { ContextMenuModule } from 'primeng/contextmenu';
import { TableModule } from 'primeng/table';
import { ComponentsModule } from '../components/components.module';


@NgModule({
  declarations: [JobsComponent],
  imports: [
    CommonModule,
    ContextMenuModule,
    ComponentsModule,
    TableModule
  ]
})
export class ETLModule { }
